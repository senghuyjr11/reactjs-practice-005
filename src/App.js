import "./App.css";
import React from "react";
import { Container, Row } from 'react-bootstrap'
import "bootstrap/dist/css/bootstrap.min.css";
import MyCard from "./components/MyCard";
import MyTable from './components/MyTable'
import MyNavbar from './components/MyNavbar'

class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      foods: [
        {
          id: 1,
          name: "Hot Lattee",
          thumbnail: "/images/hot_lattee.jpg",
          qty: 0,
          price: 1,
          total: 0,
          discount: 0,
          total1: 0,
        },
        {
          id: 2,
          name: "Ice Lattee",
          thumbnail: "/images/ice_lattee.jpg",
          qty: 0,
          price: 1.5,
          total: 0,
          discount: 0,
          total1: 0,
        },
        {
          id: 3,
          name: "Hot Cappuccino",
          thumbnail: "/images/hot_cappuccino.jpg",
          qty: 0,
          price: 1.5,
          total: 0,
          discount: 0,
          total1: 0,
        },
        {
          id: 4,
          name: "Ice Americano",
          thumbnail: "/images/ice_americano.jpg",
          qty: 0,
          price: 2,
          total: 0,
          discount: 0,
          total1: 0,
        },
      ],
    };
  }

  onAdd = (index) => {
    console.log("index:", index);
    let temp = [...this.state.foods];
    temp[index].qty++;
    temp[index].total = temp[index].qty * temp[index].price;
    temp[index].discount = 10;
    temp[index].total1 = temp[index].total - (temp[index].total * temp[index].discount / 100);
    this.setState({
      foods: temp,
    });
  };

  onDelete = (index) => {
    console.log("index:", index);
    let temp = [...this.state.foods];
    temp[index].qty--;
    temp[index].total = temp[index].qty * temp[index].price;
    temp[index].discount = 10;
    temp[index].total1 = temp[index].total - (temp[index].total * temp[index].discount / 100);
    this.setState({
      foods: temp,
    });
  };

  onClear = () => {
    console.log("Clearing...");
    let foods = [...this.state.foods]
    foods.map(item => {
      item.qty = 0
      item.total = 0
    })

    this.setState({
      foods
    })

  }

  render() {
    return (
      <Container>
        <MyNavbar />
        
        <Row>
          <MyCard
            foods={this.state.foods}
            onAdd={this.onAdd}
            onDelete={this.onDelete}
          />
        </Row>
        <br />
        <Row>
          <MyTable foods={this.state.foods} onClear={this.onClear} />
        </Row>
      </Container>
    );
  }
}

export default App;
