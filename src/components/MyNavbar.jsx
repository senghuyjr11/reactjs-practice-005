import React, { Component } from 'react';
import { Navbar } from "react-bootstrap";

export default class MyNavbar extends Component {
    render() {
        return (
            <>
            <Navbar>
                <Navbar.Brand href="#home">Web Practice 005</Navbar.Brand>
                <Navbar.Toggle />
                <Navbar.Collapse className="justify-content-end">
                    <Navbar.Text>
                        Signed in as: <a href="#login">Senghuy</a>
                    </Navbar.Text>
                </Navbar.Collapse>
            </Navbar><hr/>
            </>
        )
    }
}
