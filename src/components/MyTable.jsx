import React from 'react';
import { Table, Button, Col, Row } from 'react-bootstrap'

export default function MyTable({ foods, onClear }) {
    console.log("Props on func: ", foods);

    let temp = foods.filter(item => {
        return item.qty > 0
    })

    return (
        <>
        <Col>
            <Row>
                <Button style={{marginRight: 10 + 'px'}} onClick={onClear} variant="warning">Clear all</Button>
                <h2> {temp.length} Foods</h2>
            </Row>
            <Row>
            <Table style={{marginTop: 10 + 'px'}} striped bordered hover>
          <thead>
            <tr>
              <th>#</th>
              <th>name</th>
              <th>Quantity</th>
              <th>Price</th>
              <th>Sub-Total</th>
              <th>Discount</th>
              <th>Grand-Total</th>
            </tr>
          </thead>
          <tbody>
            {temp.map((item, index) => (
              <tr>
                <td>{index+1}</td>
                <td>{item.name}</td>
                <td>{item.qty}</td>
                <td>{item.price}</td>
                <td>$ {item.total}</td>
                <td>% {item.discount}</td>
                <td>$ {item.total1}</td>
              </tr>
            ))}
          </tbody>
        </Table>
        </Row>
        </Col>
 
       
        </>
      );
}
