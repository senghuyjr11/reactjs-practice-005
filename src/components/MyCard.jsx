import React, { Component } from 'react'
import { Card, Button, Col } from "react-bootstrap";

class MyCard extends Component {
    render() {
        return (
            <>
                {this.props.foods.map((item, index) => (
                    <Col xs="3" key={index}>
                        <Card>
                            <Card.Img variant="top" width="200px" height="250px" src={item.thumbnail} />
                            <Card.Body>
                                <Card.Title>{item.name}</Card.Title>
                                <Card.Text>
                                    Price: {item.price} $
                                </Card.Text>
                                <Button disabled={item.qty === 0} onClick={() => this.props.onDelete(index)} variant="danger">Delete</Button>{' '}
                                <Button onClick={() => this.props.onAdd(index)} variant="primary">Add</Button>{' '}
                                <Button variant="warning">{item.qty}</Button>
                                <h2>Total: {item.total} $</h2>
                            </Card.Body>
                        </Card>
                    </Col>
                )
                )}

            </>
        )
    }
}

export default MyCard;